"use strict";

function Furiganize(src) {
    var sources = src.split('|'),
        base    = sources.shift().split(''),
        text    = sources.join('|').split('|'),
        buf     = [];

    buf.push('<ruby>');
    if ( base.length == text.length ) {
        for ( var i = 0, len = base.length; i < len; i++ ) {
            buf.push( base[i], '<rt>', text[i] ,'</rt>' );
        }
    } else {
        buf.push( base.join(''), '<rt>', text.join('') ,'</rt>' );
    }
    buf.push('</ruby>');

    return buf.join('');
}

module.exports = {
    filters: {
        ruby: Furiganize,
        furigana: Furiganize
    }
};
