#!/usr/bin/env node

var t = require('assert');
var f = require('./index.js');

t.equal(f.filters.ruby, f.filters.furigana);

t.equal(
    f.filters.ruby('約束されし勝利の剣|エクスカリバー'),
    '<ruby>約束されし勝利の剣<rt>エクスカリバー</rt></ruby>'
);

t.equal(
    f.filters.furigana('電子書籍|でん|し|しょ|せき'),
    '<ruby>電<rt>でん</rt>子<rt>し</rt>書<rt>しょ</rt>籍<rt>せき</rt></ruby>'
);
